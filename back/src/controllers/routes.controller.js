const jsonwebtoken = require('jsonwebtoken')
const User = require('../models/user')
require('dotenv').config('src/.env')

const getUser = (req, res)=>{
    res.json({message: 'get up'})
}

const postUser = async(req, res)=>{
    const {email, password} = req.body

    const newUser = new User({email, password})
    await newUser.save()

   const tokenProvider = jsonwebtoken.sign({_id: newUser._id},process.env.SECRET)
    res.status(200).json({tokenProvider});

}

const postSignIn = async (req, res) =>{
    const {email, password} = req.body
    const user = await User.findOne({email})

    if(!user) return res.status(404).send('email dosent exist')
    if(user.password !== password) return res.status(401).send('Wrong password')

    const generateToken = jsonwebtoken.sign({_id: user._id}, process.env.SECRET);
    return res.status(404).json({generateToken})

}

const publicTask = (req,res)=>{
    res.json([{
        _id: 1,
        name: 'Tarea 1',
        desc: 'Lorem Ipsum',
        date: '2020-22-03'
    },
    {
        _id: 2,
        name: 'Tarea 2',
        desc: 'Lorem Ipsum',
        date: '2020-22-03'
    },
    {
        _id: 3,
        name: 'Tarea 3',
        desc: 'Lorem Ipsum',
        date: '2020-22-03'
    }
])
}

const privateTask = (req, res, next)=>{
    res.json([{
        _id: 1,
        name: 'Tarea 1',
        desc: 'Lorem Ipsum',
        date: '2020-22-03'
    },
    {
        _id: 2,
        name: 'Tarea 2',
        desc: 'Lorem Ipsum',
        date: '2020-22-03'
    },
    {
        _id: 3,
        name: 'Tarea 3',
        desc: 'Lorem Ipsum',
        date: '2020-22-03'
    }
])

}



module.exports = {
    getUser,
    postUser,
    postSignIn,
    publicTask,
    privateTask
}
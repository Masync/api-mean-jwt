const jsonwebtoken = require('jsonwebtoken');
require('dotenv').config({path: 'src/.env'});

const verification = (req, res, next) =>{
    const token = req.headers['authorization'];
    if (!token) {
        return res.status(401).send('no token provided');
    }
   const decode = jsonwebtoken.verify(token,process.env.SECRET);
    req.userId = decode.id;
    next();
}


module.exports = {verification}

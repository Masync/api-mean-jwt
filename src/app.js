require('dotenv').config({path: 'src/.env'})
require('./bdconect')
const app = require('./index')

async function main() {
    app.set('port', process.env.PORT)
    await app.listen(app.get('port'))
}

main()


const {Router} = require('express')
const routes = Router()
const {getUser,postUser,postSignIn,publicTask,privateTask} = require('../controllers/routes.controller')
const {verification} = require('../controllers/verifyToken')

routes.get('/home',getUser)
routes.get('/task', publicTask)
routes.get('/taskPrivate',verification, privateTask)
routes.post('/signUp',postUser)
routes.post('/signIn',postSignIn)


module.exports = routes